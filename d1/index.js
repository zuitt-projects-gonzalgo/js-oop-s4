class Student {
	constructor(name, email, grades){
		this.name = name
		this.email = email
		this.gradeAve = undefined
		this.passed = undefined
		this.passedWithHonors = undefined

		if(grades.length === 4){
			if(grades.every(grade => typeof grade === "number")){
				if(grades.every(grade => grade >= 0 && grade <= 100)){
					this.grades = grades
				}else{
					this.grades = undefined
				}				
			}else{
				this.grades = undefined
			}
		}else{
			this.grades = undefined
		}
	}

	login(){
		console.log(`${this.email} has logged in`)
		return this
	}

	logout(){
		console.log(`${this.email} has logged out`)
		return this
	}

	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
		return this
	}

	computeAve(){
		let sum = 0
		this.grades.forEach(grade => sum = sum + grade)
		//when called, computeAve will assign the computed value to each object's gradeAve property
		this.gradeAve = sum/4
		return this;
	}

	willPass(){
		this.passed = this.computeAve().gradeAve >= 85 ? true : false
		return this
	}

	willPassWithHonors(){
		this.computeAve()
		
		if(this.gradeAve >= 90){
			this.passedWithHonors = true
		}else if(this.gradeAve < 90 && this.gradeAve >= 85){
			this.passedWithHonors = false
		}else{
			this.passedWithHonors = undefined
		}

		return this
	}

}

class Section {
	constructor(name) {
		this.name = name
		this.students = []
		this.honorStudents = undefined
		this.honorsPercentage = undefined

	}

	addStudent(name, email, grades) {
		this.students.push(new Student(name, email, grades))
	}

	countHonorStudents() {
		//count the number of honor students, starting with zero
		let count = 0
		//use forEach() to loop through our students array
		this.students.forEach(student => {
			//call the willPassWithHonors to give a value to passedWithHonors
			if(student.willPassWithHonors().passedWithHonors) {
				//it true increment the count by 1
				count++
			}
		})
		//assign the value of count to honorStudents property
		this.honorStudents = count
		//return the section object so that methods are chainable
		return this
	}

	computeHonorsPercentage() {
		this.honorsPercentage = (this.countHonorStudents().honorStudents / this.students.length) * 100 + "%"
		return this
	}
}

const section1A = new Section('section1A')
section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88])
section1A.addStudent("Joe", "joe@mail.com", [78, 82, 79, 85])
section1A.addStudent("Jane", "jane@mail.com", [87, 89, 91, 93])
section1A.addStudent("Jessie", "jessie@mail.com", [91, 89, 92, 93])
console.log(section1A)