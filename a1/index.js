class Student {
	constructor(name, email, grades){
		this.name = name
		this.email = email
		this.gradeAve = undefined
		this.passed = undefined
		this.passedWithHonors = undefined

		if(grades.length === 4){
			if(grades.every(grade => typeof grade === "number")){
				if(grades.every(grade => grade >= 0 && grade <= 100)){
					this.grades = grades
				}else{
					this.grades = undefined
				}				
			}else{
				this.grades = undefined
			}
		}else{
			this.grades = undefined
		}
	}

	login(){
		console.log(`${this.email} has logged in`)
		return this
	}

	logout(){
		console.log(`${this.email} has logged out`)
		return this
	}

	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
		return this
	}

	computeAve(){
		let sum = 0
		this.grades.forEach(grade => sum = sum + grade)
		//when called, computeAve will assign the computed value to each object's gradeAve property
		this.gradeAve = sum/4
		return this;
	}

	willPass(){
		this.passed = this.computeAve().gradeAve >= 85 ? true : false
		return this
	}

	willPassWithHonors(){
		this.computeAve()
		
		if(this.gradeAve >= 90){
			this.passedWithHonors = true
		}else if(this.gradeAve < 90 && this.gradeAve >= 85){
			this.passedWithHonors = false
		}else{
			this.passedWithHonors = undefined
		}

		return this
	}

}

class Section {
	constructor(name) {
		this.name = name
		this.students = []
		this.honorStudents = undefined
		this.honorsPercentage = undefined

	}

	addStudent(name, email, grades) {
		this.students.push(new Student(name, email, grades))
	}

	countHonorStudents() {
		//count the number of honor students, starting with zero
		let count = 0
		//use forEach() to loop through our students array
		this.students.forEach(student => {
			//call the willPassWithHonors to give a value to passedWithHonors
			if(student.willPassWithHonors().passedWithHonors) {
				//it true increment the count by 1
				count++
			}
		})
		//assign the value of count to honorStudents property
		this.honorStudents = count
		//return the section object so that methods are chainable
		return this
	}

	computeHonorsPercentage() {
		this.honorsPercentage = (this.countHonorStudents().honorStudents / this.students.length) * 100 + "%"
		return this
	}
}

// Define a Grade class whose constructor will accept a number argument to serve as its grade level. It will have the following properties: DONE

// level initialized to passed in number argument
// sections initialized to an empty array
// totalStudents initialized to zero
// totalHonorStudents initialized to zero
// batchAveGrade set to undefined
// batchMinGrade set to undefined
// batchMaxGrade set to undefined

class Grade {
	constructor(lvl) {
		this.lvl = lvl
		this.sections = []
		this.totalStudents = 0
		this.totalHonorStudents = 0
		this.batchAveGrade = undefined
		this.batchMinGrade = undefined
		this.batchMaxGrade = undefined
	}

	addSection(sectionName) {
		this.sections.push(new Section(sectionName))
		return this
	}

	countStudents() {
		this.totalStudents = 0
		this.sections.forEach(section => this.totalStudents = this.totalStudents + section.students.length)
		return this
	}

	countHonorStudents() {
		this.sections.forEach(section => this.totalHonorStudents = this.totalHonorStudents + section.countHonorStudents().honorStudents
				)
		return this

	}

	computeBatchAve() {
		let sum = 0
		this.countStudents()

		this.sections.forEach(section => {
			section.students.forEach(student => {
				sum = sum + student.computeAve().gradeAve
			})
		})

		this.batchAveGrade = sum / this.totalStudents

		return this
	}

	getBatchMinGrade() {
		let min

		this.sections.forEach(section => {
			section.students.forEach(student => {
				student.grades.forEach(grade => {
					if(typeof min === 'undefined' || grade < min){
						min = grade
					}
				})
			})
		})

		this.batchMinGrade = min
		return this
	}

	getBatchMaxGrade() {
		let max

		this.sections.forEach(section => {
			section.students.forEach(student => {
				student.grades.forEach(grade => {
					if(typeof max === 'undefined' || grade > max){
						max = grade
					}
				})
			})
		})

		this.batchMaxGrade = max
		return this
	}
}


// Define an addSection() method that will take in a string argument to instantiate a new Section object and push it into the sections array property.
//DONE


// BEFORE proceeding, instantiate a grade level called grade1 and add the following sections in it:

//instantiate new Grade object
const grade1 = new Grade(1);

//add sections to this grade level
grade1.addSection('section1A');
grade1.addSection('section1B');
grade1.addSection('section1C');
grade1.addSection('section1D');


// BEFORE proceeding, populate the 4 sections with the following test data:

//save sections of this grade level as constants
const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

//populate the sections with students
section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);


// Define a countStudents() method that will iterate over every section in the grade level, incrementing the totalStudents property of the grade level object for every student found in every section.


// Define a countHonorStudents() method that will perform similarly to countStudents() except that it will only consider honor students when incrementing the totalHonorStudents property.


// Define a computeBatchAve() method that will get the average of all the students' grade averages and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation.


// Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student of this grade level regardless of section.


// Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade scored by a student of this grade level regardless of section.
